"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import {Admin}  from '../mongoTypes/admin';
const admin_1 = require("../mongoTypes/admin");
const posting_1 = require("../mongoTypes/posting");
async function ping(telegram, msg) {
    const chatId = msg.chat.id;
    if (telegram.isAdmin(chatId)) {
        await telegram.bot.sendMessage(chatId, 'yo');
    }
    else {
        console.log(`access violation for ${chatId}`);
    }
}
async function getPosting(telegram, msg) {
    const chatId = msg.chat.id;
    if (telegram.isAdmin(chatId)) {
        const postings = await posting_1.PostingModel.find();
        let result = '';
        for (const posting of postings) {
            result += `${posting.name}:\n`;
            result += `\`\`\`\n${posting.fromType} ${posting.from}(${posting.fromName}) --> `;
            result += `${posting.toType} ${posting.to}(${posting.toName}), `;
            result += `${!posting.cloak ? 'not ' : ''}cloaked \n\`\`\`\n`;
        }
        if (result !== '') {
            await telegram.bot.sendMessage(chatId, result, {
                parse_mode: 'Markdown'
            });
        }
        else {
            await telegram.bot.sendMessage(chatId, 'empty');
        }
    }
    else {
        console.log(`access violation for ${chatId}`);
    }
}
async function addPostingHelp(telegram, msg) {
    const chatId = msg.chat.id;
    if (telegram.isAdmin(chatId)) {
        let message = '```\n';
        message += '/addPosting MyPosting chat 87654321 FromName chat 32145687 ToName 0\n';
        message += '```\n';
        message += '`MyPosting` - posting uniq name\n';
        message += '`chat` - stream type: "chat" or "queue"\n';
        message += '`87654321` \\ `32145687` - posting source \\ destination: "TelegramChatId" for "chat" and "QueueId" for "queue"\n';
        message += '`FromName` \\ `ToName` - posting source \\ destination name\n';
        message += '`0` - cloak original source "1" \\ "0". Optional parameter. Default is "1"\n';
        await telegram.bot.sendMessage(chatId, message, {
            parse_mode: 'Markdown'
        });
    }
    else {
        console.log(`access violation for ${chatId}`);
    }
}
async function delPostingHelp(telegram, msg) {
    const chatId = msg.chat.id;
    if (telegram.isAdmin(chatId)) {
        let message = '```\n';
        message += '/delPosting MyPosting\n';
        message += '```\n';
        message += '`MyPosting` - posting name\n';
        await telegram.bot.sendMessage(chatId, message, {
            parse_mode: 'Markdown'
        });
    }
    else {
        console.log(`access violation for ${chatId}`);
    }
}
async function addAdminHelp(telegram, msg) {
    const chatId = msg.chat.id;
    if (telegram.isAdmin(chatId)) {
        await telegram.bot.sendMessage(chatId, '```\n/addAdmin 12345678 AdminName\n```\n`12345678` - Telegram ID\n`AdminName` - Any name', {
            parse_mode: 'Markdown'
        });
    }
    else {
        console.log(`access violation for ${chatId}`);
    }
}
async function getPostingHelp(telegram, msg) {
    const chatId = msg.chat.id;
    if (telegram.isAdmin(chatId)) {
        await telegram.bot.sendMessage(chatId, '```\n/getPosting\n```', {
            parse_mode: 'Markdown'
        });
    }
    else {
        console.log(`access violation for ${chatId}`);
    }
}
async function addAdmin(telegram, msg, telegramId, name) {
    const chatId = msg.chat.id;
    if (telegram.isAdmin(chatId)) {
        try {
            const newAdmin = await admin_1.AdminModel.create({ telegramId, name });
            telegram.admins.push(newAdmin);
            await telegram.bot.sendMessage(chatId, `Admin "${name}"(${telegramId}) added`);
        }
        catch (e) {
            if (e.code === 11000) {
                await telegram.bot.sendMessage(chatId, '"name" \\ "telegramId" is not unique', {
                    parse_mode: 'Markdown'
                });
            }
            else {
                console.error(e);
            }
        }
    }
    else {
        console.log(`access violation for ${chatId}`);
    }
}
async function addPosting(telegram, msg, name, fromType, from, fromName, toType, to, toName, cloak) {
    const chatId = msg.chat.id;
    if (telegram.isAdmin(chatId)) {
        try {
            const newPosting = await posting_1.PostingModel.create({ name, fromType, from, fromName, toType, to, toName, cloak });
            telegram.postings.push(newPosting);
            await telegram.bot.sendMessage(chatId, `Posting "${name}" added`);
        }
        catch (e) {
            if (e.code === 11000) {
                await telegram.bot.sendMessage(chatId, '"name" is not unique', {
                    parse_mode: 'Markdown'
                });
            }
            else {
                console.error(e);
            }
        }
    }
    else {
        console.log(`access violation for ${chatId}`);
    }
}
async function delPosting(telegram, msg, name) {
    const chatId = msg.chat.id;
    if (telegram.isAdmin(chatId)) {
        try {
            await posting_1.PostingModel.deleteOne({ name });
            telegram.postings = telegram.postings.filter((x) => x.name === name);
            await telegram.bot.sendMessage(chatId, `Posting "${name}" deleted`);
        }
        catch (e) {
            if (e.code === 11000) {
                await telegram.bot.sendMessage(chatId, 'Unable to delete posting "name"', {
                    parse_mode: 'Markdown'
                });
            }
            else {
                console.error(e);
            }
        }
    }
    else {
        console.log(`access violation for ${chatId}`);
    }
}
// async function echo(telegram: Telegram, msg: TelegramBot.Message) {
//   const chatId = msg.chat.id;
//   if (telegram.isAdmin(chatId)) {
//     return;
//   }
//   try {
//     telegram.dialog = {
//       type: 'none'
//     };
//     await telegram.bot.sendMessage(chatId, msg.text);
//   } catch (e) {
//     await telegram.bot.sendMessage(chatId, 'offline');
//   }
// }
// async function initEcho(telegram: Telegram, msg: TelegramBot.Message) {
//   const chatId = msg.chat.id;
//   if (chatId !== telegram.telegramId) {
//     return;
//   }
//   try {
//     telegram.dialog = {
//       type: 'theDialog'
//     };
//     await telegram.bot.sendMessage(chatId, 'send some text');
//   } catch (e) {
//     await telegram.bot.sendMessage(chatId, e.message);
//   }
// }
// async function messageProcessing(telegram: Telegram, msg: TelegramBot.Message) {
//     if (telegram.dialog.type === 'theDialog') {
//       await echo(telegram, msg);
//     }
// }
async function messageProcessing(telegram, msg) {
    console.log(msg);
    const from = msg.chat.id;
    const messageId = msg.message_id;
    const text = msg.text;
    const postings = telegram.postings.filter((x) => x.from === from);
    for (const posting of postings) {
        if (posting.cloak) {
            if (msg.photo) {
                const bestImage = msg.photo.sort((photo1, photo2) => photo1.file_size - photo2.file_size)[0];
                await telegram.bot.sendPhoto(posting.to, bestImage.file_id);
            }
            if (msg.video) {
                await telegram.bot.sendVideo(posting.to, msg.video.file_id);
            }
            if (msg.audio) {
                await telegram.bot.sendAudio(posting.to, msg.audio.file_id);
            }
            if (msg.document) {
                await telegram.bot.sendDocument(posting.to, msg.document.file_id);
            }
            if (msg.text) {
                await telegram.bot.sendMessage(posting.to, text);
            }
        }
        else {
            if (posting.toType === 'chat') {
                await telegram.bot.forwardMessage(posting.to, from, messageId);
            }
        }
    }
}
function telegramCommandProcessing(telegram) {
    telegram.bot.onText(/^\/ping$/, (msg) => ping(telegram, msg));
    telegram.bot.onText(/^\/getPosting$/, (msg) => getPosting(telegram, msg));
    telegram.bot.onText(/^\/addAdmin$/, (msg) => addAdminHelp(telegram, msg));
    telegram.bot.onText(/^\/addPosting$/, (msg) => addPostingHelp(telegram, msg));
    telegram.bot.onText(/^\/delPosting$/, (msg) => delPostingHelp(telegram, msg));
    telegram.bot.onText(/^\/help$/, (msg) => Promise.all([
        addAdminHelp(telegram, msg), addPostingHelp(telegram, msg), delPostingHelp(telegram, msg), getPostingHelp(telegram, msg)
    ]));
    telegram.bot.onText(/^\/addAdmin (\d+) ([a-zA-Z0-9]+)$/, (msg, match) => addAdmin(telegram, msg, parseInt(match[1], 10), match[2]));
    telegram.bot.onText(/^\/delPosting ([a-zA-Z0-9]+)$/, (msg, match) => delPosting(telegram, msg, match[1]));
    // Add posting
    telegram.bot.onText(/^\/addPosting\s+([a-zA-Z0-9]+)\s+chat\s+(-?\d+)\s+([a-zA-Z0-9]+)\s+chat\s+(-?\d+)\s+([a-zA-Z0-9]+)\s*([0-1]?)$/, (msg, match) => addPosting(telegram, msg, match[1], 'chat', parseInt(match[2], 10), match[3], 'chat', parseInt(match[4], 10), match[5], match[6] === '1'));
    // telegram.bot.onText(/^\/echo$/, (msg: TelegramBot.Message) => initEcho(telegram, msg));
    telegram.bot.on('channel_post', (msg) => messageProcessing(telegram, msg));
    telegram.bot.on('message', (msg) => messageProcessing(telegram, msg));
}
exports.default = telegramCommandProcessing;
//# sourceMappingURL=telegramCommandProcessing.js.map