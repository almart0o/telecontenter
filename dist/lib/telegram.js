"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_telegram_bot_api_1 = __importDefault(require("node-telegram-bot-api"));
class Telegram {
    /**
     * telegram lib
     * @param telegramToken notification bot telegram token
     * @param telegramId    telegram user id for notifications
     */
    constructor(telegramToken, admins, postings, commandProcessor) {
        this.bot = new node_telegram_bot_api_1.default(telegramToken, {
            polling: true,
        });
        this.dialog = {
            type: 'none'
        };
        this.admins = admins;
        this.postings = postings;
        commandProcessor(this);
    }
    /**
     * send telegram message
     * @param  message message's text
     */
    // async sendMessage(telegramId: number, message: string): Promise<void> {
    //   await this.bot.sendMessage(telegramId, message);
    // }
    isAdmin(telegramId) {
        return this.admins.find((a) => a.telegramId === telegramId) !== undefined;
    }
}
exports.default = Telegram;
//# sourceMappingURL=telegram.js.map