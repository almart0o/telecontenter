"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("config"));
const mongoose_1 = __importDefault(require("mongoose"));
const telegram_1 = __importDefault(require("./lib/telegram"));
const telegramCommandProcessing_1 = __importDefault(require("./lib/telegramCommandProcessing"));
const admin_1 = require("./mongoTypes/admin");
const posting_1 = require("./mongoTypes/posting");
const teleContenterConfig = config_1.default.get('teleContenter');
const db = config_1.default.get('db');
const main = (async () => {
    await mongoose_1.default.connect(db);
    const admins = await admin_1.AdminModel.find();
    const postings = await posting_1.PostingModel.find();
    const telegram = new telegram_1.default(teleContenterConfig.telegramToken, admins, postings, (_telegram) => telegramCommandProcessing_1.default(_telegram));
    // await telegram.sendMessage('teleContenter is online');
    console.log(telegram.admins);
    console.log('teleContenter is online');
});
main().catch(console.error);
//# sourceMappingURL=server.js.map