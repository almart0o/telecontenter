"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typegoose_1 = require("typegoose");
const SourceTypes = {
    chat: 'chat',
    queue: 'queue',
};
class Posting extends typegoose_1.Typegoose {
}
__decorate([
    typegoose_1.prop({
        required: true,
        unique: true
    }),
    __metadata("design:type", String)
], Posting.prototype, "name", void 0);
__decorate([
    typegoose_1.prop({
        enum: Object.values(SourceTypes)
    }),
    __metadata("design:type", String)
], Posting.prototype, "fromType", void 0);
__decorate([
    typegoose_1.prop({
        required: true
    }),
    __metadata("design:type", Object)
], Posting.prototype, "from", void 0);
__decorate([
    typegoose_1.prop({
        enum: Object.values(SourceTypes)
    }),
    __metadata("design:type", String)
], Posting.prototype, "toType", void 0);
__decorate([
    typegoose_1.prop({
        required: true
    }),
    __metadata("design:type", Object)
], Posting.prototype, "to", void 0);
__decorate([
    typegoose_1.prop({
        required: true
    }),
    __metadata("design:type", String)
], Posting.prototype, "fromName", void 0);
__decorate([
    typegoose_1.prop({
        required: true
    }),
    __metadata("design:type", String)
], Posting.prototype, "toName", void 0);
__decorate([
    typegoose_1.prop({
        default: true
    }),
    __metadata("design:type", Boolean)
], Posting.prototype, "cloak", void 0);
exports.Posting = Posting;
exports.PostingModel = new Posting().getModelForClass(Posting);
//# sourceMappingURL=posting.js.map