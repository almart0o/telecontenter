import  TelegramBot from 'node-telegram-bot-api';
import {Admin}  from '../mongoTypes/admin';
import {Posting}  from '../mongoTypes/posting';
interface Dialig {
    type: string;
    args?: any;
}

export default class Telegram {
  bot: TelegramBot;
  admins: Admin[];
  postings: Posting[];
  commandProcessor: (telegram: Telegram) => void;
  dialog: Dialig;
  /**
   * telegram lib
   * @param telegramToken notification bot telegram token
   * @param telegramId    telegram user id for notifications
   */
   constructor(telegramToken: string, admins: Admin[], postings: Posting[], commandProcessor:  (bot: Telegram) => void) {
     this.bot = new TelegramBot(telegramToken, {
       polling: true,
     });
     this.dialog = {
       type: 'none'
     };
     this.admins = admins;
     this.postings = postings;
     commandProcessor(this);
   }
  /**
   * send telegram message
   * @param  message message's text
   */
  // async sendMessage(telegramId: number, message: string): Promise<void> {
  //   await this.bot.sendMessage(telegramId, message);
  // }
  isAdmin(telegramId: number) {
    return this.admins.find((a: Admin) => a.telegramId === telegramId) !== undefined;
  }
}
