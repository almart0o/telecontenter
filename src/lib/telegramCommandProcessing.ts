import Telegram from './telegram';
import TelegramBot from 'node-telegram-bot-api';
// import {Admin}  from '../mongoTypes/admin';
import {AdminModel}  from '../mongoTypes/admin';
import {Posting}  from '../mongoTypes/posting';
import {PostingModel}  from '../mongoTypes/posting';

async function ping(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (telegram.isAdmin(chatId)) {
    await telegram.bot.sendMessage(chatId, 'yo');
  } else {
    console.log(`access violation for ${chatId}`);
  }
}
async function getPosting(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (telegram.isAdmin(chatId)) {
    const postings = await PostingModel.find();
    let result = '';
    for (const posting of postings) {
      result += `${posting.name}:\n`;
      result += `\`\`\`\n${posting.fromType} ${posting.from}(${posting.fromName}) --> `;
      result += `${posting.toType} ${posting.to}(${posting.toName}), `;
      result += `${!posting.cloak ? 'not ' : ''}cloaked \n\`\`\`\n`;
    }
    if (result !== '') {
      await telegram.bot.sendMessage(chatId, result, {
        parse_mode: 'Markdown'
      });
    } else {
      await telegram.bot.sendMessage(chatId, 'empty');
    }
  } else {
    console.log(`access violation for ${chatId}`);
  }
}
async function addPostingHelp(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (telegram.isAdmin(chatId)) {
    let message = '```\n';
    message += '/addPosting MyPosting chat 87654321 FromName chat 32145687 ToName 0\n';
    message += '```\n';
    message += '`MyPosting` - posting uniq name\n';
    message += '`chat` - stream type: "chat" or "queue"\n';
    message += '`87654321` \\ `32145687` - posting source \\ destination: "TelegramChatId" for "chat" and "QueueId" for "queue"\n';
    message += '`FromName` \\ `ToName` - posting source \\ destination name\n';
    message += '`0` - cloak original source "1" \\ "0". Optional parameter. Default is "1"\n';
    await telegram.bot.sendMessage(chatId, message, {
        parse_mode: 'Markdown'
      });
  } else {
    console.log(`access violation for ${chatId}`);
  }
}
async function delPostingHelp(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (telegram.isAdmin(chatId)) {
    let message = '```\n';
    message += '/delPosting MyPosting\n';
    message += '```\n';
    message += '`MyPosting` - posting name\n';
    await telegram.bot.sendMessage(chatId, message, {
        parse_mode: 'Markdown'
      });
  } else {
    console.log(`access violation for ${chatId}`);
  }
}
async function addAdminHelp(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (telegram.isAdmin(chatId)) {
    await telegram.bot.sendMessage(chatId,
      '```\n/addAdmin 12345678 AdminName\n```\n`12345678` - Telegram ID\n`AdminName` - Any name', {
        parse_mode: 'Markdown'
      });
  } else {
    console.log(`access violation for ${chatId}`);
  }
}
async function getPostingHelp(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (telegram.isAdmin(chatId)) {
    await telegram.bot.sendMessage(chatId,
      '```\n/getPosting\n```', {
        parse_mode: 'Markdown'
      });
  } else {
    console.log(`access violation for ${chatId}`);
  }
}
async function addAdmin(telegram: Telegram, msg: TelegramBot.Message, telegramId: number, name: string) {
  const chatId = msg.chat.id;
  if (telegram.isAdmin(chatId)) {
    try {
      const newAdmin = await AdminModel.create({telegramId, name});
      telegram.admins.push(newAdmin);
      await telegram.bot.sendMessage(chatId, `Admin "${name}"(${telegramId}) added`);
    } catch (e) {
      if (e.code === 11000) {
        await telegram.bot.sendMessage(chatId, '"name" \\ "telegramId" is not unique', {
            parse_mode: 'Markdown'
          });
      } else {
        console.error(e);
      }
    }
  } else {
    console.log(`access violation for ${chatId}`);
  }
}
async function addPosting(telegram: Telegram, msg: TelegramBot.Message, name: string,
  fromType: string, from: string | number, fromName: string,
  toType: string, to: string | number, toName: string, cloak?: boolean) {
  const chatId = msg.chat.id;
  if (telegram.isAdmin(chatId)) {
    try {
      const newPosting = await PostingModel.create({name, fromType, from, fromName, toType, to, toName, cloak});
      telegram.postings.push(newPosting);
      await telegram.bot.sendMessage(chatId, `Posting "${name}" added`);
    } catch (e) {
      if (e.code === 11000) {
        await telegram.bot.sendMessage(chatId, '"name" is not unique',  {
            parse_mode: 'Markdown'
          });
      } else {
        console.error(e);
      }
    }
  } else {
    console.log(`access violation for ${chatId}`);
  }
}
async function delPosting(telegram: Telegram, msg: TelegramBot.Message, name: string) {
  const chatId = msg.chat.id;
  if (telegram.isAdmin(chatId)) {
    try {
      await PostingModel.deleteOne({name});
      telegram.postings = telegram.postings.filter((x: Posting) => x.name === name);
      await telegram.bot.sendMessage(chatId, `Posting "${name}" deleted`);
    } catch (e) {
      if (e.code === 11000) {
        await telegram.bot.sendMessage(chatId, 'Unable to delete posting "name"',  {
            parse_mode: 'Markdown'
          });
      } else {
        console.error(e);
      }
    }
  } else {
    console.log(`access violation for ${chatId}`);
  }
}
// async function echo(telegram: Telegram, msg: TelegramBot.Message) {
//   const chatId = msg.chat.id;
//   if (telegram.isAdmin(chatId)) {
//     return;
//   }
//   try {
//     telegram.dialog = {
//       type: 'none'
//     };
//     await telegram.bot.sendMessage(chatId, msg.text);
//   } catch (e) {
//     await telegram.bot.sendMessage(chatId, 'offline');
//   }
// }

// async function initEcho(telegram: Telegram, msg: TelegramBot.Message) {
//   const chatId = msg.chat.id;
//   if (chatId !== telegram.telegramId) {
//     return;
//   }
//   try {
//     telegram.dialog = {
//       type: 'theDialog'
//     };
//     await telegram.bot.sendMessage(chatId, 'send some text');
//   } catch (e) {
//     await telegram.bot.sendMessage(chatId, e.message);
//   }
// }


// async function messageProcessing(telegram: Telegram, msg: TelegramBot.Message) {
//     if (telegram.dialog.type === 'theDialog') {
//       await echo(telegram, msg);
//     }
// }
async function messageProcessing(telegram: Telegram, msg: TelegramBot.Message) {
  console.log(msg);
  const from = msg.chat.id;
  const messageId = msg.message_id;
  const text = msg.text;
  const postings = telegram.postings.filter((x: Posting) => x.from === from);
  for (const posting of postings) {
    if (posting.cloak) {
      if (msg.photo) {
        const bestImage = msg.photo.sort((photo1: TelegramBot.File, photo2: TelegramBot.File) => photo1.file_size - photo2.file_size)[0];
        await telegram.bot.sendPhoto(posting.to, bestImage.file_id);
      }
      if (msg.video) {
        await telegram.bot.sendVideo(posting.to, (msg.video as any).file_id);
      }
      if (msg.audio) {
        await telegram.bot.sendAudio(posting.to, (msg.audio as any).file_id);
      }
      if (msg.document) {
        await telegram.bot.sendDocument(posting.to, (msg.document as any).file_id);
      }
      if (msg.text) {
        await telegram.bot.sendMessage(posting.to, text);
      }
    }  else {
      if (posting.toType === 'chat') {
        await telegram.bot.forwardMessage(posting.to, from, messageId);
      }
    }
  }
}

export default function telegramCommandProcessing(telegram: Telegram): void {
  telegram.bot.onText(/^\/ping$/, (msg: TelegramBot.Message) => ping(telegram, msg));
  telegram.bot.onText(/^\/getPosting$/, (msg: TelegramBot.Message) => getPosting(telegram, msg));
  telegram.bot.onText(/^\/addAdmin$/, (msg: TelegramBot.Message) => addAdminHelp(telegram, msg));
  telegram.bot.onText(/^\/addPosting$/, (msg: TelegramBot.Message) => addPostingHelp(telegram, msg));
  telegram.bot.onText(/^\/delPosting$/, (msg: TelegramBot.Message) => delPostingHelp(telegram, msg));
  telegram.bot.onText(/^\/help$/, (msg: TelegramBot.Message) => Promise.all([
    addAdminHelp(telegram, msg), addPostingHelp(telegram, msg), delPostingHelp(telegram, msg), getPostingHelp(telegram, msg)
  ]));
  telegram.bot.onText(/^\/addAdmin (\d+) ([a-zA-Z0-9]+)$/,
    (msg: TelegramBot.Message, match: RegExpMatchArray) => addAdmin(telegram, msg, parseInt(match[1], 10), match[2]));
  telegram.bot.onText(/^\/delPosting ([a-zA-Z0-9]+)$/,
    (msg: TelegramBot.Message, match: RegExpMatchArray) => delPosting(telegram, msg, match[1]));
  // Add posting
  telegram.bot.onText(/^\/addPosting\s+([a-zA-Z0-9]+)\s+chat\s+(-?\d+)\s+([a-zA-Z0-9]+)\s+chat\s+(-?\d+)\s+([a-zA-Z0-9]+)\s*([0-1]?)$/,
    (msg: TelegramBot.Message, match: RegExpMatchArray) => addPosting(telegram, msg, match[1],
      'chat',  parseInt(match[2], 10), match[3],
      'chat',  parseInt(match[4], 10), match[5],
      match[6] === '1'
    ));
  // telegram.bot.onText(/^\/echo$/, (msg: TelegramBot.Message) => initEcho(telegram, msg));
  telegram.bot.on('channel_post', (msg: TelegramBot.Message) => messageProcessing(telegram, msg));
  telegram.bot.on('message', (msg: TelegramBot.Message) => messageProcessing(telegram, msg));
}
