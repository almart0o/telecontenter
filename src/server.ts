import config from 'config';
import mongoose from 'mongoose';
import Telegram  from './lib/telegram';
import telegramCommandProcessing  from './lib/telegramCommandProcessing';
import {AdminModel}  from './mongoTypes/admin';
import {PostingModel}  from './mongoTypes/posting';
const teleContenterConfig = config.get<TeleContenterConfig>('teleContenter');
const db = config.get<string>('db');

const main = (async () => {
  await mongoose.connect(db);
  const admins = await AdminModel.find();
  const postings = await PostingModel.find();
  const telegram = new Telegram(teleContenterConfig.telegramToken, admins, postings,
      (_telegram: Telegram) => telegramCommandProcessing(_telegram));
  // await telegram.sendMessage('teleContenter is online');
  console.log(telegram.admins);
  console.log('teleContenter is online');
});
main().catch(console.error);
