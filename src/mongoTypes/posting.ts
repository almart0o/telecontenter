import { prop, Typegoose } from 'typegoose';

type SourceType = 'chat' | 'queue';
const SourceTypes = {
  chat: 'chat' as SourceType,
  queue: 'queue' as SourceType,
};
export class Posting extends Typegoose {
  @prop({
    required: true,
    unique: true
  })
  name: string;
  @prop({
    enum: Object.values(SourceTypes)
  })
  fromType: string;
  @prop({
    required: true
  })
  from: string | number;
  @prop({
    enum: Object.values(SourceTypes)
  })
  toType: string;
  @prop({
    required: true
  })
  to: string | number;
  @prop({
    required: true
  })
  fromName: string;
  @prop({
    required: true
  })
  toName: string;
  @prop({
    default: true
  })
  cloak: boolean;
}

export const PostingModel = new Posting().getModelForClass(Posting);
