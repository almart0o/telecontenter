import { prop, Typegoose } from 'typegoose';

export class Admin extends Typegoose {
  @prop({
    required: true,
    unique: true
  })
  telegramId: number;
  @prop({
    required: true,
    unique: true
  })
  name: string;
}

export const AdminModel = new Admin().getModelForClass(Admin);
