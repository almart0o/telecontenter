FROM node:8

WORKDIR /usr/src/telecontenter
RUN mkdir ./config
COPY package*.json ./
COPY dist ./

RUN npm install


CMD [ "node", "server.js" ]
